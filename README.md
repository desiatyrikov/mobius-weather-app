# Weather App

Write a web application in a language, state management system, and framework of your choice (For example, React + Redux) to display the local weather of a location given to you by a user.  The expectation is to submit a github repo as an example of how you code and develop.

You can use the openweather api found here: https://openweathermap.org/api

There should be a basic login page.  This does not need to hit an actual backend for API authorization, just fake a basic auth and then use the api token from the openweather api system.

The user should be able to enter a city, and the app should output what the current weather of that city is.  It should have basic art to display what the current weather is.  It should be able to save the most recent searches and autocomplete to a recent search.

A city to longitude and latitude api is available in the same api (https://openweathermap.org/api/geocoding-api) and current weather (https://openweathermap.org/current)

Bonus is to show a 7 day forecast and be able to pin cities to track multiple of them at the same time.

# The following are expected of any codebase submission
* Enforcement of a coding style
* unit and api testing
* Documentation when the code is not sufficient
* A github repository with commits or pull requests demonstrating feature development stages
* Project and development completion is not the most important part.  A well documented, well developed, well maintained submission that does not do everything listed will be better than an unmaintainable codebase.
