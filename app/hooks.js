import { useSelector } from "react-redux"

import store from "./store"

export const useAppDispatch = () => store.dispatch
export const useAppSelector = useSelector