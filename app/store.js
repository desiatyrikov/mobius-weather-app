import { combineReducers, configureStore } from "@reduxjs/toolkit"
import watch from "redux-watch"
import { fetchForecastAsync, forecastSlice } from "../features/forecast/forecastSlice"
import { locationSlice } from "../features/location/locationSlice"
import { fetchCurrentWeatherAsync, weatherSlice } from "../features/weather/weatherSlice"

const makeStore = () => {
    return configureStore({
        reducer: combineReducers({
            weather: weatherSlice.reducer,
            location: locationSlice.reducer,
            forecast: forecastSlice.reducer,
        })
    })
}

const store = makeStore()

const updateWeather = () => {
    const state = store.getState();

    const location = state.location.location;

    if (location && location.lat && location.lon) {
        store.dispatch(fetchCurrentWeatherAsync({
            lat: location.lat,
            lon: location.lon
        }));

        store.dispatch(fetchForecastAsync({
            lat: location.lat,
            lon: location.lon
        }));
    }
}

const watch_location = watch(store.getState, 'location.location');

store.subscribe(watch_location((newVal, oldVal) => updateWeather()));

export default store