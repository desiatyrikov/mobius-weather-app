import { Card, Col, Container, Image, Row, Stack } from "react-bootstrap";

export default function ForecastTile({ forecast, measures }) {

    return (
        <Card bg="dark" text="light">
            <Card.Body>
                <Container>
                    <Row className="justify-content-md-center">
                        {
                            forecast.map((entry) => {
                                return (
                                    <ForecastWeatherCol

                                        key={entry.dt}
                                        forecast={entry}
                                        measures={measures}
                                    />
                                )
                            })
                        }
                    </Row>
                </Container>
            </Card.Body>
        </Card>
    );
}

function ForecastWeatherCol({ forecast, measures }) {
    const temperature = parseInt(forecast.main?.temp);
    const description = forecast.weather[0]?.description;
    const icon = forecast.weather[0]?.icon;
    const date = new Date(forecast.dt * 1000).toDateString();

    return (
        <Col lg={2} >
            <Stack direction="vertical" gap={3}>
                <Container className="forecast">
                    <ForecastDate value={date} />
                </Container>
                <Container className="forecast">
                    <ForecastTemperature value={temperature} units={measures.temperature} />
                </Container>
                <Container className="forecast forecast_description">
                    {description}
                </Container>
                <Container className="forecast forecast_icon">
                    <ForecastWeatherIcon value={icon} />
                </Container>
            </Stack>
        </Col>
    );
}

function ForecastWeatherIcon({ value }) {
    return (
        <Image src={`https://openweathermap.org/img/wn/${value}@2x.png`} />
    );
}

function ForecastTemperature({ value, units }) {
    return (
        <h2>
            <b>
                {`${value > 0 ? "+" : ""}${value}${units}`}
            </b>
        </h2>
    );
}

function ForecastDate({ value }) {
    return (
        <h5>{value}</h5>
    );
}