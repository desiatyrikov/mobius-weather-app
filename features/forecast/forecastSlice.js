import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchForecast } from "./forecastApi";

const initialState = {
    status: "idle",
    list: []
};

/**
 * fetchForecast Async Thunk
 */
export const fetchForecastAsync = createAsyncThunk(
    'forecast/fetchForecast',
    async ({ lat, lon, lang = "en", units = "imperial" }) => {

        let payload = {};
        try {
            const response = await fetchForecast(lat, lon, lang, units);
            payload = response;
        } catch (error) {
            console.error(error);
        }

        return payload;
    }
)

export const forecastSlice = createSlice({

    name: 'forecast',
    initialState: initialState,

    reducers: {
        /**
         * Remove forecast object from the list by city id
         * @param {*} state 
         * @param {*} action 
         */
        removeForecastById: (state, action) => {
            const id = action.payload;
            state.list = state.list.filter((entry) => entry.id != id);
        }
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchForecastAsync.pending, (state) => {
                state.status = "loading";
            })
            .addCase(fetchForecastAsync.fulfilled, (state, action) => {

                if (action.payload.city) {

                    const forecast = {
                        id: action.payload.city.id,
                        list: action.payload.list.filter(
                            //since we receive forecasts with 3 hour step
                            //we are going to select only 12 o'clock forecast for each day
                            (entry) => new Date(entry.dt * 1000).getUTCHours() == 12)
                    };

                    //search for the index by the city id
                    const index = state.list.findIndex((entry) => entry.id == forecast.id);

                    if (index === -1) {
                        //push new forecast object if it is not in list
                        state.list.push(forecast);
                    } else {
                        //replase the forecast object by city id
                        state.list = state.list.map(
                            (entry, i) => i === index ? forecast : entry);
                    }
                }

                state.status = "ready";

                return state;
            });
    }
})

export const { removeForecastById } = forecastSlice.actions
export const selectForecastStatus = (state) => state.forecast.status;
export const selectForecastList = (state) => state.forecast.list;
export const selectForecast = (state, id) => state.forecast.list.find(
    (entry) => entry.id == id
);

export default forecastSlice.reducer