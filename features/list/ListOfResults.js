import { Card, CloseButton, Container, Spinner, Stack } from "react-bootstrap";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { removeWeatherById, selectWeatherList, selectWeatherStatus } from "../weather/weatherSlice";
import ForecastTile from "../forecast/ForecastTile";
import WeatherTile from "../weather/WeatherTile";
import { removeForecastById, selectForecastList } from "../forecast/forecastSlice";

export default function ListOfResults({ }) {

    const dispatch = useAppDispatch();

    const status = useAppSelector(selectWeatherStatus);
    const weather_results = useAppSelector(selectWeatherList);
    const forecast_results = useAppSelector(selectForecastList);

    const measures = {
        imperial: {
            temperature: "°F",
            humidity: "%",
            wind_speed: "mph",
            pressure: "hPa"
        },
        metric: {
            temperature: "°C",
            humidity: "%",
            wind_speed: "m/s",
            pressure: "hPa"
        },
        standard: {
            temperature: "°K",
            humidity: "%",
            wind_speed: "m/s",
            pressure: "hPa"
        }
    };

    const handleCloseClick = (id) => {
        dispatch(removeWeatherById(id));
        dispatch(removeForecastById(id));
    }

    return (
        <Container>
            <Stack direction="vertical">
                {
                    status != "ready" ?
                        <Card className="spinner_card">
                            <Card.Body>
                                <Spinner animation="border" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </Spinner>
                            </Card.Body>
                        </Card> :
                        <></>

                }
                {
                    weather_results.map((weather) => {
                        return (
                            <Result
                                key={weather.id}
                                cityId={weather.id}
                                weather={weather}
                                forecast={
                                    forecast_results.find((forecast) =>
                                        forecast.id == weather.id
                                    )?.list ?? []
                                }
                                measures={measures.imperial}
                                showHeader={weather_results.length > 1}
                                onClose={handleCloseClick}
                            />
                        );
                    }).reverse()
                }
            </Stack>
        </Container>
    );
}

function Result({
    cityId,
    weather,
    forecast,
    measures,
    showHeader,
    onClose
}) {

    return (
        <Card>
            <Card.Header className="location_header" hidden={!showHeader}>
                <CloseButton onClick={() => onClose(cityId)} />
            </Card.Header>
            <Card.Body>
                <Stack direction="vertical">
                    <WeatherTile
                        weather={weather}
                        measures={measures}
                    />
                    <ForecastTile
                        forecast={forecast}
                        measures={measures}
                    />
                </Stack>
            </Card.Body>
        </Card>
    );
}