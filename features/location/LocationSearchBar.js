import { useRef, useState } from "react";
import { Button, ButtonGroup, Dropdown } from "react-bootstrap";

import {
    useAppDispatch,
    useAppSelector
} from "../../app/hooks";

import {
    fetchLocationByNameAsync,
    selectLocationList,
    selectLocation,
    setLocationById,
    resetLocation
} from "./locationSlice";

export default function LocationSearchBar() {

    const timeoutIdRef = useRef();
    const [dropdownOpen, setDropdownOpen] = useState(false);

    const [value, setValue] = useState("");
    const dispatch = useAppDispatch();
    const location = useAppSelector(selectLocation);
    const list = useAppSelector(selectLocationList);

    const getTitle = (c) => {
        if (c && c.name) {
            if (c.state)
                return `${c.name}, ${c.state}, ${c.country}`
            else
                return `${c.name}, ${c.country}`;

        }
        return "";
    };

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    const handleKeyDown = (event) => {
        if (event.key === "Enter") {
            search();
        }
    }

    const search = () => {

        showDropdown();

        const nameToSearch = value.trim();

        if (nameToSearch.length > 2 &&
            nameToSearch.length < 30 &&
            list.findIndex((entry) => entry.name == nameToSearch) == -1
        ) {
            dispatch(fetchLocationByNameAsync(nameToSearch));
        }
    }

    const handleSelect = (id) => {
        hideDropdown();
        dispatch(resetLocation());
        dispatch(setLocationById(id));
    };

    const showDropdown = () => {
        clearTimeout(timeoutIdRef.current);
        setDropdownOpen(true);
    };

    const hideDropdown = (delay) => {
        clearTimeout(timeoutIdRef.current);
        if (delay) {
            timeoutIdRef.current = setTimeout(() => setDropdownOpen(false), delay);
        } else {
            setDropdownOpen(false);
        }
    };

    return (

        <Dropdown
            onSelect={handleSelect}
            show={dropdownOpen}
            onBlur={() => hideDropdown(500)}
        >
            <ButtonGroup>
                <input
                    type="search"
                    maxLength={30}
                    placeholder="Type here to search"
                    defaultValue={getTitle(location)}
                    onChange={handleChange}
                    onKeyDown={handleKeyDown}
                    onFocus={() => showDropdown()}
                />
                <Button onClick={(e) => { search(); }}>
                    Search
                </Button>
            </ButtonGroup>

            <Dropdown.Menu >
                {
                    list.map((entry) => {
                        if (entry.name.toLowerCase().indexOf(value.toLowerCase().trim()) != -1) {
                            return (
                                <Dropdown.Item
                                    key={entry.id}
                                    eventKey={entry.id}>
                                    {
                                        getTitle(entry)
                                    }
                                </Dropdown.Item>
                            );
                        }
                    })
                }
            </Dropdown.Menu>
        </Dropdown>
    );
}