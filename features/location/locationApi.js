import {
    OWM_API_KEY,
    OWM_API_URL
} from "../../app/const";

/**
 * Get current geolocation by invoking navigator.geolocation.getCurrentPosition
 * 
 * @param {boolean} enableHighAccuracy 
 * @param {number} maximumAge 
 * @param {number} timeout 
 * @returns {Promise} that will be resolved with rosition object 
 * or rejected with geolocation api error if the location access wan not permitted or declined
 */
export async function getGeolocation(
    enableHighAccuracy = true,
    maximumAge = 0,
    timeout = Infinity,
) {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(
            (position) => resolve(position),
            (error) => reject(error),
            {
                enableHighAccuracy: enableHighAccuracy,
                maximumAge: maximumAge,
                timeout: timeout,
            }
        );
    });
}

/**
 * Fetch current location by receiving geolocation 
 * and reverse coding through openweathermap geo api
 * 
 * @returns {Object} location object
 * @throws error if api not available or response is incorrect
 */
export async function fetchCurrentLocation() {
    let position = await getGeolocation();

    return await fetchLocationByCoords(position.coords.latitude, position.coords.longitude);
}

/**
 * Fetch current location by reverse coding through openweathermap geo api
 * 
 * @param {string} name - search query
 * @param {number} limit - number of possible results
 * @returns {Object} location object
 * @throws error if api not available or response is incorrect
 */
export async function fetchLocationByName(name, limit = 5) {
    try {
        const response = await fetch(
            `${OWM_API_URL}/geo/1.0/direct` +
            `?appid=${OWM_API_KEY}` +
            `&q=${name}` +
            `&limit=${limit}`
        )

        if (response.status == 200) {
            const data = await response.json();
            return data;
        } else {
            throw `Unable to fetch location. Status: ${response.status}`;
        }
    } catch (error) {
        throw `Unable to fetch location. ${error.message}`;
    }
}

/**
 * Fetch current location by reverse coding through openweathermap geo api
 * 
 * @param {number} lat - latitude
 * @param {number} lon - longitude
 * @returns {Array} list of location objects
 * @throws error if api not available or response is incorrect
 */
export async function fetchLocationByCoords(lat, lon) {
    try {
        const response = await fetch(
            `${OWM_API_URL}/geo/1.0/reverse` +
            `?appid=${OWM_API_KEY}` +
            `&lat=${lat}` +
            `&lon=${lon}`
        )

        if (response.status == 200) {
            const data = await response.json();
            return data;
        } else {
            throw `Unable to fetch location. Status: ${response.status}`;
        }
    } catch (error) {
        throw `Unable to fetch location. ${error.message}`;
    }
}