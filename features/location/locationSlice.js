import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchCurrentLocation, fetchLocationByName } from "./locationApi";

const initialState = {
    status: "idle",
    location: undefined,
    currentLocation: undefined,
    list: []
};

/**
 * fetchCurrentLocation Async Thunk
 */
export const fetchCurrentLocationAsync = createAsyncThunk(
    'location/fetchCurrentLocation',
    async () => {
        let list = [];
        try {
            const response = await fetchCurrentLocation();
            //response should be an array
            //if not - return an empty array
            list = Array.isArray(response) ? response : list;
        } catch (error) {
            console.error(error);
        }
        return list;
    }
)

/**
 * fetchCurrentLocation Async Thunk
 * @param {string} name - location name to search
 */
export const fetchLocationByNameAsync = createAsyncThunk(
    'location/fetchLocationByName',
    async (name) => {
        let list = [];
        try {
            const response = await fetchLocationByName(name);
            //response should be an array
            //if not - return an empty array
            list = Array.isArray(response) ? response : [];
        } catch (error) {
            console.error(error);
        }
        return list;
    }
)

export const locationSlice = createSlice({

    name: 'location',
    initialState: initialState,

    reducers: {
        resetLocation: (state, action) => {
            state.location = undefined;
        },
        setLocationById: (state, action) => {
            const id = action.payload;

            //try to find location objest in the list by its id
            const location = state.list.find((entry) => entry.id == id);

            if (location !== undefined) {
                state.location = location;
            }
        },
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchCurrentLocationAsync.pending, (state) => {
                state.status = "loading";
            })
            .addCase(fetchCurrentLocationAsync.fulfilled, (state, action) => {

                if (action.payload.length > 0) {
                    const location = action.payload[0];
                    const id = `${location.lat}:${location.lon}`;

                    location.id = id;

                    state.currentLocation = location;
                    state.location = location;
                    state.list.push(location);
                }

                state.status = "ready";
            })
            .addCase(fetchLocationByNameAsync.pending, (state) => {
                state.status = "loading";
            })
            .addCase(fetchLocationByNameAsync.fulfilled, (state, action) => {
                action.payload.forEach((entry) => {

                    const id = `${entry.lat}:${entry.lon}`;

                    //try to find entry with the same id
                    const index = state.list.findIndex((entry) => entry.id == id);

                    //push only new cities
                    if (index === -1) {
                        entry.id = id;

                        //keep the list no bigger than 50 elements
                        if (state.list.length > 50) {
                            state.list.shift()
                        }

                        state.list.push(entry);
                    }

                });
                state.status = "ready";
            });
    }
});

export const { setLocationById, resetLocation } = locationSlice.actions

export const selectLocation = (state) => state.location.location;
export const selectCurrentLocation = (state) => state.location.currentLocation;
export const selectLocationList = (state) => state.location.list;

export default locationSlice.reducer