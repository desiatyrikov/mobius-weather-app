
import { Card, Col, Container, Image, Row } from 'react-bootstrap';

export default function WeatherTile({ weather, measures }) {

    const name = weather.name;
    const country = weather.sys.country;
    const temperature = weather.main.temp;
    const temperatureMax = weather.main.temp_max;
    const temperatureMin = weather.main.temp_min;
    const humidity = weather.main.humidity;
    const pressure = weather.main.pressure;
    const windSpeed = weather.wind.speed;
    const description = weather.weather[0]?.description;
    const icon = weather.weather[0]?.icon;

    const getTitle = () => {
        if (name) {
            return `${name}, ${country}`;
        }
        return "";
    };

    return (
        <Card>
            <Card.Body>
                <Card.Title>{getTitle()}</Card.Title>

                <Row className="justify-content-md-center">

                    <Col>
                        <div>
                            <Temperature value={temperature} units={measures.temperature} />
                        </div>
                        <div>
                            {description}
                        </div>
                    </Col>
                    <Col>
                        <div className="weather_icon">
                            <WeatherIcon value={icon} />
                        </div>
                    </Col>
                    <Col>
                        <h5><b>Details:</b></h5>
                        <div>
                            <Humidity value={humidity} units={measures.humidity} />
                        </div>
                        <div>
                            <TemperatureHighLow high={temperatureMax} low={temperatureMin} units={measures.temperature} />
                        </div>
                        <div>
                            <WindSpeed value={windSpeed} units={measures.wind_speed} />
                        </div>
                        <div>
                            <Pressure value={pressure} units={measures.pressure} />
                        </div>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    );
}

function WeatherIcon({ value }) {
    return (
        <Image src={`https://openweathermap.org/img/wn/${value}@4x.png`} />
    );
}

function Temperature({ value, units }) {
    return (
        <h1 id="temp">
            {`${value > 0 ? "+" : ""}${value}${units}`}
        </h1>
    );
}

function TemperatureHighLow({ high, low, units }) {
    return (
        <span id="temp_high_low">
            <b>High/Low: </b>
            {`${high > 0 ? "+" : ""}${high}${units}`}
            <span> / </span>
            {`${low > 0 ? "+" : ""}${low}${units}`}
        </span >
    );
}


function Humidity({ value, units }) {
    return (
        <span id="humidity">
            <b>Humidity:</b> {value} {units}
        </span >
    );
}

function Pressure({ value, units }) {
    return (
        <span id="pressure">
            <b>Pressure:</b> {value} {units}
        </span >
    );
}

function WindSpeed({ value, units }) {
    return (
        <span id="windSpeed">
            <b>Wind:</b> {value} {units}
        </span>
    );
}