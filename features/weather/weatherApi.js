import {
    OWM_API_KEY,
    OWM_API_URL
} from "../../app/const";

/**
 * Fetch current weather from the api.openweathermap.org by the coordinates
 * @param {number} lat - latitude
 * @param {number} lon - longitude
 * @param {string} lang - default "en"
 * @param {string} units - imperial, metric or empty to use standard units
 * @returns weather result object
 */
export async function fetchCurrentWeather(lat, lon, lang = "en", units = "imperial") {

    try {
        const response = await fetch(
            `${OWM_API_URL}/data/2.5/weather` +
            `?appid=${OWM_API_KEY}` +
            `&lat=${lat}` +
            `&lon=${lon}` +
            `&lang=${lang}` +
            `&units=${units}`
        );

        if (response.status == 200) {
            const data = await response.json();
            return data;
        } else {
            throw `Unable to fetch current weather. \nStatus: ${response.status}`
        }
    } catch (error) {
        throw `Unable to fetch current weather. \n${error.message}`
    }
}