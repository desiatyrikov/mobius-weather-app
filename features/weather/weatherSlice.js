import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { fetchCurrentWeather } from './weatherApi';

const initialState = {
    status: "idle",
    list: []
};

/**
 * fetchCurrentWeather Async Thunk
 */
export const fetchCurrentWeatherAsync = createAsyncThunk(
    'weather/fetchCurrentWeather',
    async ({ lat, lon, lang = "en", units = "imperial" }) => {

        let payload = {};
        try {
            const response = await fetchCurrentWeather(lat, lon, lang, units);

            //parse response to make sure it's valid
            payload = {
                id: response?.id,
                name: response?.name,
                main: response?.main ? {
                    temp: parseInt(response.main.temp),
                    feels_like: parseInt(response.main.feels_like),
                    humidity: parseInt(response.main.humidity),
                    pressure: parseInt(response.main.pressure),
                    temp_min: parseInt(response.main.temp_min),
                    temp_max: parseInt(response.main.temp_max),
                } : {},
                sys: response?.sys ?? {},
                weather: response?.weather ?? {},
                wind: response?.wind ?? {},
                coord: response?.coord ?? {},
            };
        } catch (error) {
            console.error(error);
        }

        return payload;
    }
)

export const weatherSlice = createSlice({

    name: 'weather',
    initialState: initialState,

    reducers: {
        /**
         * Remove weather object from the list by city id
         * @param {*} state 
         * @param {*} action 
         */
        removeWeatherById: (state, action) => {
            const id = action.payload;
            state.list = state.list.filter((entry) => entry.id != id);
        }
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchCurrentWeatherAsync.pending, (state) => {
                state.status = "loading";
            })
            .addCase(fetchCurrentWeatherAsync.fulfilled, (state, action) => {
                if (action.payload.id) {

                    const weather = action.payload;

                    //search for the index by the city id
                    const index = state.list.findIndex((entry) => entry.id == weather.id);

                    if (index === -1) {
                        //push new weather object if it is not in list
                        state.list.push(weather);
                    } else {
                        //replase the weather object by city id
                        state.list = state.list.map(
                            (entry, i) => i === index ? weather : entry);
                    }

                }
                state.status = "ready";
            });
    }
})

export const { removeWeatherById } = weatherSlice.actions
export const selectWeatherList = (state) => state.weather.list;
export const selectWeatherStatus = (state) => state.weather.status;


export default weatherSlice.reducer