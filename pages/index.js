import 'bootstrap/dist/css/bootstrap.css';
import { useSession, signIn, signOut } from "next-auth/react";
import { useEffect } from 'react';
import { Button, Container, Modal, Nav, Navbar } from 'react-bootstrap';
import { useAppDispatch } from '../app/hooks';
import ListOfResults from '../features/list/ListOfResults';
import LocationSearchBar from '../features/location/LocationSearchBar';
import { fetchCurrentLocationAsync } from '../features/location/locationSlice';

function Header() {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>Weather App</Navbar.Brand>
        <Nav className="ms-auto">
          <Nav.Item>
            <LocationSearchBar />
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onClick={signOut}>Sign out</Nav.Link>
          </Nav.Item>
        </Nav>
      </Container>
    </Navbar>
  );
}

function Content() {
  return (
    <>
      <ListOfResults />
    </>
  );
}


function Home() {

  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(fetchCurrentLocationAsync());
  }, []);

  return (
    <>
      <Header />
      <Content />
    </>
  )
}

function SignIn() {
  return (
    <Modal.Dialog>

      <Modal.Body>
        <p>You are not authorized. Please sign in.</p>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="primary" onClick={signIn}>Sign in</Button>
      </Modal.Footer>
    </Modal.Dialog>

  );
}

export default function Component() {
  const { data: session } = useSession()

  if (session) {
    return (
      <Home />
    )
  }

  return <SignIn />
}
