import { fetch as fetchPolyfill } from 'whatwg-fetch';
import XMLHttpRequest from 'xhr2';
import { fetchForecast } from '../features/forecast/forecastApi';

global.fetch = fetchPolyfill;
global.XMLHttpRequest = XMLHttpRequest;

test("should reseive 5-days forecast for New York, NY", async () => {

    // these are coordinates of the New York, NY
    const lat = 40.713;
    const lon = -74.0072;

    const forecast = await fetchForecast(lat, lon);

    expect(forecast.city.name).toEqual("New York");
    expect(forecast.list).toBeInstanceOf(Array);
    expect(forecast.list[0].main).toBeDefined();
    expect(forecast.list[0].weather[0].description).toBeDefined();
    expect(forecast.list[0].weather[0].icon).toBeDefined();
});
