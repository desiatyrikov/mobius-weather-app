import { combineReducers, configureStore } from '@reduxjs/toolkit';
import * as forecastAPI from '../features/forecast/forecastApi';
import reducer, {
    fetchForecastAsync,
    removeForecastById
} from '../features/forecast/forecastSlice'

const mockStore = () => configureStore({
    reducer: combineReducers({
        forecast: reducer
    })
});

test('should handle removing forecast from the list by id', () => {
    const previousState = {
        status: "idle",
        list: [{
            id: 1
        }]
    };

    expect(reducer(previousState, removeForecastById(1))).toEqual({
        status: "idle",
        list: []
    });
})

it('should set forecast.list[0]', async () => {

    const store = mockStore();
    const forecast = {
        city: {
            id: 1001,
        },
        list: []
    }

    jest.spyOn(forecastAPI, 'fetchForecast').mockImplementation(() =>
        Promise.resolve(forecast));

    await store.dispatch(fetchForecastAsync({
        lat: 0,
        lon: 0
    }));

    const state = store.getState();

    expect(state.forecast.list[0].id).toEqual(1001);
})

it('should set forecast.list[0] and forecast.list[1]', async () => {
    const store = mockStore();
    const forecasts = [
        {
            city: {
                id: 1001,
            },
            list: []
        },
        {
            city: {
                id: 1002,
            },
            list: []
        }
    ];

    jest.spyOn(forecastAPI, 'fetchForecast').mockImplementation(() =>
        Promise.resolve(forecasts[0]));

    await store.dispatch(fetchForecastAsync({
        lat: 0,
        lon: 0
    }));

    jest.spyOn(forecastAPI, 'fetchForecast').mockImplementation(() =>
        Promise.resolve(forecasts[1]));

    await store.dispatch(fetchForecastAsync({
        lat: 0,
        lon: 0
    }));

    const state = store.getState();

    expect(state.forecast.list[0].id).toEqual(1001);
    expect(state.forecast.list[1].id).toEqual(1002);
})