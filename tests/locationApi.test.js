import { fetch as fetchPolyfill } from 'whatwg-fetch';
import XMLHttpRequest from 'xhr2';
import {
    fetchLocationByCoords,
    fetchLocationByName,
    getGeolocation
} from "../features/location/locationApi";

global.fetch = fetchPolyfill;
global.XMLHttpRequest = XMLHttpRequest;

test("should reseive current position object", async () => {
    const position = {
        coords: {
            latitude: 40.713,
            longitude: -74.0072
        }
    };

    global.navigator = {
        geolocation: {
            getCurrentPosition: (success, error) => {
                success(position);
            }
        }
    };

    const pos = await getGeolocation();

    expect(pos).toEqual(position);

    global.navigator = undefined;
});

test("should reseive New York location from the api by lat and lon", async () => {

    // these are coordinates of the New York, NY
    const lat = 40.713;
    const lon = -74.0072;

    const location = await fetchLocationByCoords(lat, lon);

    expect(location[0].name).toEqual("New York");

});

test("should reseive New York location from the api by name", async () => {

    // these are coordinates of the New York, NY
    const q = "New York";

    const location = await fetchLocationByName(q);

    expect(location[0].name).toEqual("New York");

});
