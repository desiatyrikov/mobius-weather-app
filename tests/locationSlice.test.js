import { combineReducers, configureStore } from '@reduxjs/toolkit';
import * as locationAPI from '../features/location/locationApi';
import reducer, {
    fetchCurrentLocationAsync,
    fetchLocationByNameAsync
} from '../features/location/locationSlice'

const mockStore = () => configureStore({
    reducer: combineReducers({
        location: reducer
    })
});

it('should set location.currentLocation', async () => {

    const store = mockStore()
    const location = [
        {
            id: 1,
            name: "New York",
            lat: 100,
            lon: 100
        }
    ];

    jest.spyOn(locationAPI, 'fetchCurrentLocation')
        .mockImplementation(() => Promise.resolve(location));

    await store.dispatch(fetchCurrentLocationAsync());

    const state = store.getState();

    expect(state.location.currentLocation.id).toEqual("100:100");
    expect(state.location.currentLocation.name).toEqual("New York");

    expect(state.location.location.id).toEqual("100:100");
    expect(state.location.location.name).toEqual("New York");

    expect(state.location.list[0].id).toEqual("100:100");
    expect(state.location.list[0].name).toEqual("New York");

})

it('should set location.list[0]', async () => {
    const store = mockStore()
    const location = [
        {
            id: 1,
            name: "New York",
            lon: -74.0072,
            lat: 40.713,
        }
    ];

    jest.spyOn(locationAPI, 'fetchLocationByName')
        .mockImplementation(() => Promise.resolve(location));

    await store.dispatch(fetchLocationByNameAsync());

    const state = store.getState();

    expect(state.location.list[0].id).toEqual("40.713:-74.0072");
    expect(state.location.list[0].name).toEqual("New York");
});