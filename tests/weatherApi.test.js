import { fetch as fetchPolyfill } from 'whatwg-fetch';
import XMLHttpRequest from 'xhr2';
import { fetchCurrentWeather } from "../features/weather/weatherApi";

global.fetch = fetchPolyfill;
global.XMLHttpRequest = XMLHttpRequest;

test("should reseive current weather in New York, NY", async () => {

    // these are coordinates of the New York, NY
    const lat = 40.713;
    const lon = -74.0072;

    const weather = await fetchCurrentWeather(lat, lon);

    expect(weather.name).toEqual("New York");
    expect(weather.main).toBeDefined();
    expect(weather.wind).toBeDefined();
    expect(weather.sys).toBeDefined();
    expect(weather.weather).toBeInstanceOf(Array);
});
