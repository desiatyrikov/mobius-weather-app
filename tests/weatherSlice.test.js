import { combineReducers, configureStore } from '@reduxjs/toolkit';
import * as weatherAPI from '../features/weather/weatherApi';
import reducer, {
    fetchCurrentWeatherAsync,
    removeWeatherById
} from '../features/weather/weatherSlice'


const mockStore = () => configureStore({
    reducer: combineReducers({
        weather: reducer
    })
});

test('should handle removing from the list by id', () => {
    const previousState = {
        status: "idle",
        list: [{
            id: 1
        }]
    };

    expect(reducer(previousState, removeWeatherById(1))).toEqual({
        status: "idle",
        list: []
    });
})

it('should set weather.list[0]', async () => {
    const store = mockStore();
    const weather = {
        "coord": {
            "lon": -122.08,
            "lat": 37.39
        },
        "weather": [
            {
                "id": 800,
                "main": "Clear",
                "description": "clear sky",
                "icon": "01d"
            }
        ],
        "base": "stations",
        "main": {
            "temp": 282,
            "feels_like": 281.86,
            "temp_min": 280.37,
            "temp_max": 284.26,
            "pressure": 1023,
            "humidity": 100
        },
        "visibility": 10000,
        "wind": {
            "speed": 1.5,
            "deg": 350
        },
        "clouds": {
            "all": 1
        },
        "dt": 1560350645,
        "sys": {
            "type": 1,
            "id": 5122,
            "message": 0.0139,
            "country": "US",
            "sunrise": 1560343627,
            "sunset": 1560396563
        },
        "timezone": -25200,
        "id": 420006353,
        "name": "Mountain View",
        "cod": 200
    }

    jest.spyOn(weatherAPI, 'fetchCurrentWeather').mockImplementation(() =>
        Promise.resolve(weather));

    await store.dispatch(fetchCurrentWeatherAsync({
        lat: 0,
        lon: 0
    }));

    const state = store.getState();

    expect(state.weather.list[0].id).toEqual(420006353);
    expect(state.weather.list[0].main.temp).toEqual(282);
    expect(state.weather.list[0].name).toEqual("Mountain View");
})
